package com.human.lw6;


import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {

    final static String DEBUG_TAG = "MakePhotoActivity";

    Button openCamera;
    Button savePhotoBtn;
    ImageView cameraReleaseBtn;
    ImageView rotateCameraBtn;
    SurfaceView surfaceView;

    Camera camera;
    Context context = MainActivity.this;
    int camId = 1;
    SurfaceHolder surfaceHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        surfaceView = findViewById(R.id.surfaceView);
        openCamera = (Button) findViewById(R.id.openCameraBtn);
        cameraReleaseBtn = (ImageView) findViewById(R.id.cameraReleaseBtn);
        rotateCameraBtn = (ImageView) findViewById(R.id.rotateCamera);
        surfaceView.setVisibility(View.GONE);         // why?
        surfaceHolder = surfaceView.getHolder();
        savePhotoBtn = (Button) findViewById(R.id.savePhoto);

    }

    public void openCamera(View view) {
        surfaceView.setVisibility(View.VISIBLE);


        try {
            camera = Camera.open(camId);
            camera.setDisplayOrientation(90);
            camera.setAutoFocusMoveCallback(null);
            camera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        openCamera.setVisibility(View.GONE);
        cameraReleaseBtn.setVisibility(View.VISIBLE);
        rotateCameraBtn.setVisibility(View.VISIBLE);
        camera.startPreview();
//        Toast.makeText(MainActivity.this, "Preview started", Toast.LENGTH_SHORT).show();
    }

    public void releasePhoto(View view) {
        rotateCameraBtn.setVisibility(View.GONE);
        cameraReleaseBtn.setVisibility(View.GONE);
        savePhotoBtn.setVisibility(View.VISIBLE);
        camera.takePicture(null, null, new PhotoHandler(getApplicationContext()));


        camera.stopPreview();
        camera.release();

        cameraReleaseBtn.setVisibility(View.GONE);
//        Toast.makeText(MainActivity.this, "Camera released", Toast.LENGTH_SHORT).show();
    }


    public void rotateCamera(View view) {
        switchCamNumber();
        Toast.makeText(MainActivity.this, "Not realized", Toast.LENGTH_SHORT).show();

        /*try {
            camera.stopPreview();

            camera = Camera.open(camId);
            camera.setDisplayOrientation(90);
            camera.setPreviewDisplay(surfaceHolder);

        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }


    private void switchCamNumber() {
        camId = (camId == 0) ? 1 : 0;
    }

    public void savePhoto(View view) {
   //     Bitmap bitmap = surfaceView.getDrawingCache();
     //   saveFile(bitmap);
    }



}
