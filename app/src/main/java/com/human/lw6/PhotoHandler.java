package com.human.lw6;

import android.content.Context;
import android.hardware.Camera;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by human on 09.04.18.
 */

public class PhotoHandler implements Camera.PictureCallback {
    private final Context context;

    public PhotoHandler(Context context) {
        this.context = context;

    }


    @Override
    public void onPictureTaken(byte[] bytes, Camera camera) {
        File pictureFileDir = getDir();

        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
            Log.d(MainActivity.DEBUG_TAG, "Can't create directory");
            Toast.makeText(context, "Can't create directory", Toast.LENGTH_LONG).show();
            return;
        }

        String photoFileName = "Picture_" + System.currentTimeMillis() + ".jpg";

        String fileName = pictureFileDir.getPath() + File.separator + photoFileName;

        File pictureFile = new File(fileName);

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(pictureFile);
            fileOutputStream.write(bytes);
            fileOutputStream.close();
            Toast.makeText(context, "New Image Saved" + photoFileName, Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            Log.d(MainActivity.DEBUG_TAG, "File" + fileName + "not saved: "
                    + e.getMessage());
            Toast.makeText(context, "Image could not be saved.",
                    Toast.LENGTH_LONG).show();
        }

    }



    private File getDir() {
        File sdDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, "LW6");
    }

}
